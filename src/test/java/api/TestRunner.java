/* Copyright (c) 2022 Automation Projects / Java */
package api;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = "src/test/java/api/features",
    glue = "api.stepdefinitions",
    plugin = {"pretty", "html:target/cucumber-reports/api/report.html"})
public class TestRunner {}
