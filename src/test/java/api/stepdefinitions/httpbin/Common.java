/* Copyright (c) 2022 Automation Projects / Java */
package api.stepdefinitions.httpbin;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import io.cucumber.java.en.Then;

public class Common {

  private final HttpBinClient client;
  private final HTTPMethods httpMethods;

  public Common(HttpBinClient client, HTTPMethods httpMethods) {
    this.client = client;
    this.httpMethods = httpMethods;
  }

  @Then("Server should return status {int}")
  public void serverShouldReturnStatus(int httpStatus) {
    int responseStatus = httpMethods.getResponse().statusCode();
    assertThat(responseStatus).isEqualTo(httpStatus);
  }
}
