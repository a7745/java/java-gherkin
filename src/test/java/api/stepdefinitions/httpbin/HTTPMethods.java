/* Copyright (c) 2022 Automation Projects / Java */
package api.stepdefinitions.httpbin;

import io.cucumber.java.en.When;
import java.io.IOException;
import java.net.http.HttpResponse;

public class HTTPMethods {

  private final HttpBinClient client;
  private HttpResponse<String> response;

  public HTTPMethods(HttpBinClient client) {
    this.client = client;
  }

  public HttpResponse<String> getResponse() {
    return response;
  }

  @When("I make a GET request to the server")
  public void iMakeAGetRequestToTheServer() throws IOException, InterruptedException {
    response = client.makeGetRequest();
  }

  @When("I make a POST request to the server")
  public void iMakeAPostRequestToTheServer() throws IOException, InterruptedException {
    response = client.makePostRequest();
  }

  @When("I make a PUT request to the server")
  public void iMakeAPutRequestToTheServer() throws IOException, InterruptedException {
    response = client.makePutRequest();
  }

  @When("I make a DELETE request to the server")
  public void iMakeADeleteRequestToTheServer() throws IOException, InterruptedException {
    response = client.makeDeleteRequest();
  }
}
