/* Copyright (c) 2022 Automation Projects / Java */
package api.stepdefinitions.httpbin;

import api.common.Client;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class HttpBinClient extends Client {

  private static final String ENDPOINT = "http://httpbin.org";

  public HttpResponse<String> makeGetRequest() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder().uri(URI.create(ENDPOINT + "/get")).build();
    return client.send(request, BodyHandlers.ofString());
  }

  public HttpResponse<String> makePostRequest() throws IOException, InterruptedException {
    HttpRequest request =
        HttpRequest.newBuilder()
            .uri(URI.create(ENDPOINT + "/post"))
            .POST(HttpRequest.BodyPublishers.noBody())
            .build();
    return client.send(request, BodyHandlers.ofString());
  }

  public HttpResponse<String> makePutRequest() throws IOException, InterruptedException {
    HttpRequest request =
        HttpRequest.newBuilder()
            .uri(URI.create(ENDPOINT + "/put"))
            .PUT(HttpRequest.BodyPublishers.noBody())
            .build();
    return client.send(request, BodyHandlers.ofString());
  }

  public HttpResponse<String> makeDeleteRequest() throws IOException, InterruptedException {
    HttpRequest request =
        HttpRequest.newBuilder().uri(URI.create(ENDPOINT + "/delete")).DELETE().build();
    return client.send(request, BodyHandlers.ofString());
  }
}
