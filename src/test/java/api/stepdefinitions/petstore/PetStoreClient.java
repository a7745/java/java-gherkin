/* Copyright (c) 2022 Automation Projects / Java */
package api.stepdefinitions.petstore;

import api.common.Client;
import api.stepdefinitions.petstore.models.User;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class PetStoreClient extends Client {

  private static final String ENDPOINT = "https://petstore.swagger.io/v2/user";

  public HttpResponse<String> login(String username, String password)
      throws IOException, InterruptedException {
    final String uri =
        String.format("%s/login?username=%s&password=%s", ENDPOINT, username, password);

    HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri)).build();
    return client.send(request, BodyHandlers.ofString());
  }

  public HttpResponse<String> logout() throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder().uri(URI.create(ENDPOINT + "/logout")).build();
    return client.send(request, BodyHandlers.ofString());
  }

  public HttpResponse<String> createUser() throws IOException, InterruptedException {
    User user = new User("johndoe", "John", "Doe", "jd@domain.com", "pass@123#", "+001122334455");
    HttpRequest request =
        HttpRequest.newBuilder()
            .uri(URI.create(ENDPOINT))
            .header("Content-Type", "application/json")
            .POST(BodyPublishers.ofString(user.asJson()))
            .build();
    return client.send(request, BodyHandlers.ofString());
  }
}
