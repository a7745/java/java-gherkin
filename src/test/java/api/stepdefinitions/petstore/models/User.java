/* Copyright (c) 2022 Automation Projects / Java */
package api.stepdefinitions.petstore.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class User {
  private Long id;
  @NonNull private String username;
  @NonNull private String firstName;
  @NonNull private String lastName;
  @NonNull private String email;
  @NonNull private String password;
  @NonNull private String phone;
  private Integer userStatus;

  public String asJson() throws JsonProcessingException {
    return new ObjectMapper().writeValueAsString(this);
  }
}
