/* Copyright (c) 2022 Automation Projects / Java */
package api.stepdefinitions.petstore;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.io.IOException;
import java.net.http.HttpResponse;

public class User {

  private final PetStoreClient client;

  private HttpResponse<String> response;

  public User(PetStoreClient client) {
    this.client = client;
  }

  @When("I login with {string} and {string}")
  public void iLoginWithAnd(String username, String password)
      throws IOException, InterruptedException {
    response = client.login(username, password);
  }

  @Then("Login should be successful")
  public void loginShouldBeSuccessful() {
    assertThat(response.statusCode()).isEqualTo(200);
    assertThat(response.body()).contains("\"code\":200");
    assertThat(response.body()).containsPattern("\"message\":\"logged in user session:\\d+\"");
  }

  @When("I logout")
  public void iLogout() throws IOException, InterruptedException {
    response = client.logout();
  }

  @Then("Logout should be successful")
  public void logoutShouldBeSuccessful() {
    assertThat(response.statusCode()).isEqualTo(200);
    assertThat(response.body()).contains("\"code\":200");
    assertThat(response.body()).contains("\"message\":\"ok\"");
  }

  @When("I create a user account with required user data")
  public void iCreateAUserAccountWithRequiredUserData() throws IOException, InterruptedException {
    response = client.createUser();
  }

  @Then("The user account should be created")
  public void theUserAccountShouldBeCreated() {
    assertThat(response.statusCode()).isEqualTo(200);
    assertThat(response.body()).containsPattern("\"message\":\"\\d+\"");
  }
}
