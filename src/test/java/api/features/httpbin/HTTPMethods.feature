Feature: Server should be able to handle various HTTP methods
  As a user, I want to make GET, POST, PUT and DELETE requests to the server

  Scenario: GET request should return status 200
    When I make a GET request to the server
    Then Server should return status 200

  Scenario: POST request should return status 200
    When I make a POST request to the server
    Then Server should return status 200

  Scenario: PUT request should return status 200
    When I make a PUT request to the server
    Then Server should return status 200

  Scenario: DELETE request should return status 200
    When I make a DELETE request to the server
    Then Server should return status 200
