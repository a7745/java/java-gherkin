Feature: User Operations
  As I Petstore user, I want to manage user accounts

  Scenario: Login
  As I user, I want to log into the Petstore
    When I login with "username" and "password"
    Then Login should be successful

  Scenario: Logout
  As I user, I want to log out from the Petstore
    Given I login with "username" and "password"
    When I logout
    Then Logout should be successful

  Scenario:  Create user
  As I user, I want to create my own user account
    When I create a user account with required user data
    Then The user account should be created
