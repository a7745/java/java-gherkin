/* Copyright (c) 2022 Automation Projects / Java */
package api.common;

import java.net.http.HttpClient;

public class Client {

  public HttpClient client = HttpClient.newBuilder().build();
}
