/* Copyright (c) 2022 Automation Projects / Java */
package web.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import web.pages.Home;

public class Login {

  private final Home home;

  public Login(Home home) {
    this.home = home;
  }

  @Given("I open the landing page")
  public void iOpenTheLandingPage() {
    home.openLandingPage();
  }

  @And("I click on the login link")
  public void iClickOnTheLoginLink() {
    home.clickLoginLink();
  }

  @Given("I enter {string} and {string}")
  public void iEnterAnd(String username, String password) {
    home.provideLoginCredentials(username, password);
  }

  @When("I click the login button")
  public void iClickTheLoginButton() {
    home.clickLoginButton();
  }

  @Then("Login should be successful")
  public void loginShouldBeSuccessful() {}
}
