/* Copyright (c) 2022 Automation Projects / Java */
package web.stepdefinitions;

import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Playwright;
import io.cucumber.java.BeforeAll;
import web.common.Base;

public class Hooks {

  @BeforeAll
  public static void setup() {
    Playwright playwright = Playwright.create();
    Base.browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
  }
}
