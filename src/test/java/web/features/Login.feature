Feature: Login
  As a web shop user, I want to be able to log in the web shop

  Background:
    Given I open the landing page
    And I click on the login link

  Scenario: Valid login
    As I user, I want to be able to log in the web shop
    Given I enter "username" and "password"
    When I click the login button
    Then Login should be successful