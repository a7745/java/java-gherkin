/* Copyright (c) 2022 Automation Projects / Java */
package web;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = "src/test/java/web/features",
    glue = "web.stepdefinitions",
    plugin = {"pretty", "html:target/cucumber-reports/web/report.html"})
public class TestRunner {}
