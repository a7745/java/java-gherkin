/* Copyright (c) 2022 Automation Projects / Java */
package web.pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import web.common.Base;

public class Home {

  public final Page page;

  public static final String HOME_URL = "https://www.demoblaze.com/index.html";
  public final Locator lnkLogin;
  public final Locator inputUsername;
  public final Locator inputPassword;
  public final Locator btnLogin;

  public final Locator txtWelcome;

  public Home() {
    this.page = Base.browser.newPage();
    this.lnkLogin = page.locator("//a[.='Log in']").first();
    this.inputUsername = page.locator("id=loginusername").first();
    this.inputPassword = page.locator("id=loginpassword").first();
    this.btnLogin = page.locator("//button[.='Log in']").first();
    this.txtWelcome = page.locator("id=nameofuser");
  }

  public void openLandingPage() {
    page.navigate(HOME_URL);
  }

  public void clickLoginLink() {
    lnkLogin.click();
  }

  public void provideLoginCredentials(String username, String password) {
    inputUsername.fill(username);
    inputPassword.fill(password);
  }

  public void clickLoginButton() {
    btnLogin.click();
  }
}
