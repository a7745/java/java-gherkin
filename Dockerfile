FROM maven:3.8.4-eclipse-temurin-16

COPY pom.xml pom.xml

RUN apt-get update && apt-get install -y xvfb && \
    rm -rf /var/lib/apt/lists/* && \
    mvn exec:java -e -Dexec.mainClass=com.microsoft.playwright.CLI -Dexec.args="install-deps"